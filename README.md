# RISC-V Encrypted Extension in LLVM

This is the RISC-V Encrypted Extension in LLVM implemented by William from Chinese Academy of Sciences.

The assembly instructions of RISC-V encrypted extension is released and defined in this depository now.


# LLVM 

The goal of this project is to support the instruction set architecture of RV32X Graphics based on LLVM. On the basis of riscv standard instruction set architecture, RV32X Graphics adds user-defined instructions, including vertex instruction subset, texture instruction subset, pixel operation instruction subset and graphics pipeline operation instruction subset. These extensions are mainly supported in the back-end project of LLVM.

## (1) The instructions supported by the implemented llvm MC assembler are listed below according to different instruction subset extensions.

                        
**AES symmetric encryption**

| Extended Subset Name      | Subset Functional Description             | Status                    |
| ------- | ---------------- | ----------------------- |
| aesenc      | AES symmetric encryption, performs multiple rounds of encryption.              | :ballot_box_with_check: |
| aesenclast    | AES symmetric encryption, used for the final round of encryption.     | :ballot_box_with_check: |
| aesdec    | AES symmetric decryption, performs multiple rounds of decryption.     | :ballot_box_with_check: |
| aesdeclast    | AES symmetric decryption, used for the final round of decryption.     | :ballot_box_with_check: |
| aesimc    | AES symmetric encryption, computes the inverse transformation during decryption.     | :ballot_box_with_check: |


**SHA-256 Secure Hash**

| instruction  | Description                 | Status                    |
| ----- | -------------------- | ----------------------- |
| sha256su0 | Hash encryption instruction, executes the round function of SHA-256.   | :ballot_box_with_check: |
| sha256su1  | Hash encryption instruction, executes the round function of SHA-256 with relatively higher parallelism.           | :ballot_box_with_check: |
| sha256h  | Hash encryption instruction, used for performing hash calculations with a 32-byte output.           | :ballot_box_with_check: |
| sha256h2  | Hash encryption instruction, used for performing hash calculations with a 16-byte output.           | :ballot_box_with_check: |


**RSA Key Generation**

| instruction    | Description             | Status                    |
| ------- | ---------------- | ----------------------- |
| rsa_key_gen    | RSA encryption instruction, used for generating RSA keys.         | :ballot_box_with_check: |
| rsa_enc    | RSA encryption instruction, used for RSA encryption operations.         | :ballot_box_with_check: |
| rsa_dec    | RSA encryption instruction, used for RSA decryption operations.         | :ballot_box_with_check: | 



**Elliptic curve encryption**

| instruction | Description                           | Status                    |
| ---- | ------------------------------ | ----------------------- |
| ecc_point_add  | Elliptic curve encryption, computes the doubling of a point on an elliptic curve. | :ballot_box_with_check: |
| ecc_point_double | Elliptic curve encryption, computes the doubling of a point on an elliptic curve.     | :ballot_box_with_check: |
| ecc_scalar_mult | Elliptic curve encryption instruction, used for calculating the scalar multiplication of a point on an elliptic curve.     | :ballot_box_with_check: |




## (2) Some control and status registers of RISC-V Encrypted Extension are extended for the processor. The following lists the registers that have been defined, including:

Machine Mode Graphics Extended Status Register (GCSR)  
Machine Mode Image Control Status Register (GHCR)  
Machine Mode Image Clear Status Register (GFLUSH)  
Machine Mode Image Set Status Register (GCINS)  
Machine Mode Image Transparency Set Status Register (GTCINS)  
Machine Mode Image Frame Buffer Status Register (GZBUFFER)  




## (3) Compile LLVM RISC-V Encrpted Extension and run tests 

**compile:**

```
$ git clone https://gitlab.com/williamwp/encrypted-compiler.git
$ cd encrypted-compiler
$ mkdir build
$ cd build
$ cmake -DLLVM_TARGETS_TO_BUILD="RISCV" -G "Unix Makefiles" ..
$ make -j $(nproc)
```

**Execute test cases:**

```
We added 3 test files for encrypted extension
$ ./encrypted-compiler/llvm/build/bin/llvm-lit -v encrypted-compiler/llvm/test/MC/RISCV/encrypted-valid.s
$ ./encrypted-compiler/llvm/build/bin/llvm-lit -v encrypted-compiler/llvm/test/MC/RISCV/encrypted-invalid.s
$ ./encrypted-compiler/llvm/build/bin/llvm-lit -v encrypted-compiler/llvm/test/MC/RISCV/machine-csr-names.s

```

The results of the implementation are as follows:

```
-- Testing: 3 tests, 3 workers --
PASS: LLVM :: MC/RISCV/encrypted-valid.s (1 of 3)
PASS: LLVM :: MC/RISCV/encrypted-invalid.s (2 of 3)
PASS: LLVM :: MC/RISCV/machine-csr-names.s (3 of 3)

Testing Time: 0.30s
  Expected Passes    : 3
```
You can also use **FileCheck** to run each test seperately:

	$ llvm-mc %s -triple=riscv32 --mattr=+encrypted -riscv-no-aliases -show-encoding \
	$  | FileCheck -check-prefixes=CHECK-ASM,CHECK-ASM-AND-OBJ %s

## (4) How to Assemble encrypted Assembly Files

    $ clang --target=riscv64-unknown-elf -S  test.c -march=rv64gcv -mabi=lp64 --sysroot=/opt/riscv64/riscv64-unknown-elf  --gcc-toolchain=/opt/riscv64  -o test.s `

```
int main(){
  int a,b,c;
  a = 1;
  b = 2;
  asm volatile
  (
    "aesenc   %[z], %[x], %[y]\n\t"
    : [z] "=r" (c)
    : [x] "r" (a), [y] "r" (b)
  );
  if ( c == 0 ){
     return -1;
  }
  return 0;
  ```

You can use **llvm-mc** to assemble encrypted assembly file and generate an object file:

	$ llvm-mc test.s -triple=riscv32 --mattr=+encrypted -riscv-no-aliases -show-encoding --filetype=obj -o test.o  
	
Note: "test.s" here means an assembly file containing riscv encrypted extension instructions, users can use clang to compile and generate test.s assembly file. 


## (5) How to Disassemble encrypted Object code

You can use **llvm-objdump** to disassemble encrypted object code file and generate assembly file:

	$ llvm-objdump test.o --mattr=+encrypted
