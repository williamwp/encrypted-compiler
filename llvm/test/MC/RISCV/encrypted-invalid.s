# RUN: not llvm-mc -triple riscv64 -mcpu=encrypted  < %s 2>&1 | FileCheck %s

# Too few operands
gather_depth a0  # CHECK: :[[@LINE]]:1: error: too few operands for instruction
gather_gradient_v a0 # CHECK: :[[@LINE]]:1: error: too few operands for instruction
get_resolution_x a0  # CHECK: :[[@LINE]]:1: error: too few operands for instruction
get_resolution_y a0  # CHECK: :[[@LINE]]:1: error: too few operands for instruction

