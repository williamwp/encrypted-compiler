# RUN: llvm-mc %s -triple=riscv64 -mcpu=encrypted -riscv-no-aliases -show-encoding \
# RUN:     | FileCheck -check-prefixes=CHECK-ASM,CHECK-ASM-AND-OBJ %s

# CHECK-ASM-AND-OBJ: gather_depth a0, a1, 4, 1
# CHECK-ASM: encoding: [0x0b,0xa5,0x15,0x10]
gather_depth a0, a1, 4, 1

# CHECK-ASM-AND-OBJ: gather_gradient_v a0, a1, 5, 1
# CHECK-ASM: encoding: [0x0b,0xb5,0x15,0x14]
gather_gradient_v a0, a1, 5, 1

# CHECK-ASM-AND-OBJ: get_resolution_x a0, a1
# CHECK-ASM: encoding: [0x0b,0x95,0x05,0x84]
get_resolution_x a0, a1

# CHECK-ASM-AND-OBJ: get_resolution_y a0, a1
# CHECK-ASM: encoding: [0x0b,0x95,0x05,0x86]
get_resolution_y a0, a1

# CHECK-ASM-AND-OBJ: get_resolution_z a0, a1
# CHECK-ASM: encoding: [0x0b,0x95,0x05,0x82]
get_resolution_z a0, a1

# CHECK-ASM-AND-OBJ: get_resolution_w a0, a1
# CHECK-ASM: encoding: [0x0b,0x95,0x05,0x90]
get_resolution_w a0, a1

# CHECK-ASM-AND-OBJ: sematic a0, a1, 1
# CHECK-ASM: encoding: [0x0b,0x95,0x25,0x88]
sematic a0, a1, 1

# CHECK-ASM-AND-OBJ: pixel_mix_buffer a1, a2, a3
# CHECK-ASM: encoding: [0x8b,0x15,0xd6,0x20]
pixel_mix_buffer a1, a2, a3

# CHECK-ASM-AND-OBJ: pixel_h a1, a2, a3
# CHECK-ASM: encoding: [0x8b,0x15,0xd6,0x28]
pixel_h a1, a2, a3

# CHECK-ASM-AND-OBJ: pixel_point a1, a2, a3
# CHECK-ASM: encoding: [0x8b,0x15,0xd6,0x22]
pixel_point a1, a2, a3

# CHECK-ASM-AND-OBJ: pixel_tri a1, a2, a3
# CHECK-ASM: encoding: [0x8b,0x15,0xd6,0x2a]
pixel_tri a1, a2, a3

# CHECK-ASM-AND-OBJ: pixel_data a1, a2, a3
# CHECK-ASM: encoding: [0x8b,0x15,0xd6,0x26]
pixel_data a1, a2, a3

# CHECK-ASM-AND-OBJ: pixel_mix a1, a2, a3
# CHECK-ASM: encoding: [0x8b,0x15,0xd6,0x40]
pixel_mix a1, a2, a3

# CHECK-ASM-AND-OBJ: pixel_buffer a1, a2, a3
# CHECK-ASM: encoding: [0x8b,0x15,0xd6,0x42]
pixel_buffer a1, a2, a3

# CHECK-ASM-AND-OBJ: pixel_srri a0, a1, 4
# CHECK-ASM: encoding: [0x0b,0x95,0x45,0x10]
pixel_srri a0, a1, 4

# CHECK-ASM-AND-OBJ: pixel_srriw a0, a1, 3
# CHECK-ASM: encoding: [0x0b,0x95,0x35,0x14]
pixel_srriw a0, a1, 3

# CHECK-ASM-AND-OBJ: pixel_op1
# CHECK-ASM: encoding: [0x0b,0x00,0x80,0x01]
pixel_op1

# CHECK-ASM-AND-OBJ: pixel_op2
# CHECK-ASM: encoding: [0x0b,0x00,0xa0,0x01]
pixel_op2

# CHECK-ASM-AND-OBJ: pixel_scatter
# CHECK-ASM: encoding: [0x0b,0x00,0xb0,0x01]
pixel_scatter

# CHECK-ASM-AND-OBJ: pixel_scratch
# CHECK-ASM: encoding: [0x0b,0x00,0x90,0x01]
pixel_scratch

# CHECK-ASM-AND-OBJ: graphics_lrb a0, a1, a2, 1
# CHECK-ASM: encoding: [0x0b,0xc5,0xc5,0x02]
graphics_lrb a0, a1, a2, 1

# CHECK-ASM-AND-OBJ: graphics_lrbu a0, a1, a2, 1
# CHECK-ASM: encoding: [0x0b,0xc5,0xc5,0x82]
graphics_lrbu a0, a1, a2, 1

# CHECK-ASM-AND-OBJ: graphics_lrd a0, a1, a2, 1
# CHECK-ASM: encoding: [0x0b,0xc5,0xc5,0x62]
graphics_lrd a0, a1, a2, 1

# CHECK-ASM-AND-OBJ: graphics_lrh a0, a1, a2, 1
# CHECK-ASM: encoding: [0x0b,0xc5,0xc5,0x22]
graphics_lrh a0, a1, a2, 1

# CHECK-ASM-AND-OBJ: graphics_lrhu a0, a1, a2, 1
# CHECK-ASM: encoding: [0x0b,0xc5,0xc5,0xa2]
graphics_lrhu a0, a1, a2, 1

# CHECK-ASM-AND-OBJ: graphics_lrw a0, a1, a2, 1
# CHECK-ASM: encoding: [0x0b,0xc5,0xc5,0x42]
graphics_lrw a0, a1, a2, 1

# CHECK-ASM-AND-OBJ: graphics_lrwu a0, a1, a2, 1
# CHECK-ASM: encoding: [0x0b,0xc5,0xc5,0xc2]
graphics_lrwu a0, a1, a2, 1

# CHECK-ASM-AND-OBJ: graphics_srd a0, a1, a2, 1
# CHECK-ASM: encoding: [0x0b,0xd5,0xc5,0x62]
graphics_srd a0, a1, a2, 1

# CHECK-ASM-AND-OBJ: graphics_srh a0, a1, a2, 1
# CHECK-ASM: encoding: [0x0b,0xd5,0xc5,0x22]
graphics_srh a0, a1, a2, 1

# CHECK-ASM-AND-OBJ: graphics_srw a0, a1, a2, 1
# CHECK-ASM: encoding: [0x0b,0xd5,0xc5,0x42]
graphics_srw a0, a1, a2, 1

# CHECK-ASM-AND-OBJ: graphics_surb a0, a1, a2, 1
# CHECK-ASM: encoding: [0x0b,0xd5,0xc5,0x12]
graphics_surb a0, a1, a2, 1

# CHECK-ASM-AND-OBJ: graphics_surd a0, a1, a2, 1
# CHECK-ASM: encoding: [0x0b,0xd5,0xc5,0x72]
graphics_surd a0, a1, a2, 1

# CHECK-ASM-AND-OBJ: graphics_surh a0, a1, a2, 1
# CHECK-ASM: encoding: [0x0b,0xd5,0xc5,0x32]
graphics_surh a0, a1, a2, 1

# CHECK-ASM-AND-OBJ: graphics_ldd a2, 0(a1)
# CHECK-ASM: encoding: [0x0b,0xc6,0x05,0xf8]
graphics_ldd a2, 0(a1)

# CHECK-ASM-AND-OBJ: graphics_lwd a2, 0(a1)
# CHECK-ASM: encoding: [0x0b,0xc6,0x05,0xe0]
graphics_lwd a2, 0(a1)

# CHECK-ASM-AND-OBJ: graphics_sdd a2, 0(a1)
# CHECK-ASM: encoding: [0x0b,0xd6,0x05,0xf8]
graphics_sdd a2, 0(a1)

# CHECK-ASM-AND-OBJ: graphics_swd a2, 0(a1)
# CHECK-ASM: encoding: [0x0b,0xd6,0x05,0xe0]
graphics_swd a2, 0(a1)


# CHECK-ASM-AND-OBJ: dvertex_call
# CHECK-ASM: encoding: [0x0b,0x00,0x10,0x00]
dvertex_call

# CHECK-ASM-AND-OBJ: dvertex_iall
# CHECK-ASM: encoding: [0x0b,0x00,0x20,0x00]
dvertex_iall

# CHECK-ASM-AND-OBJ: ivertex_iall
# CHECK-ASM: encoding: [0x0b,0x00,0x00,0x01]
ivertex_iall

# CHECK-ASM-AND-OBJ: ivertex_ialls
# CHECK-ASM: encoding: [0x0b,0x00,0x10,0x01]
ivertex_ialls

# CHECK-ASM-AND-OBJ: l2vertex_call
# CHECK-ASM: encoding: [0x0b,0x00,0x50,0x01]
l2vertex_call

# CHECK-ASM-AND-OBJ: l2vertex_ciall
# CHECK-ASM: encoding: [0x0b,0x00,0x70,0x01]
l2vertex_ciall

# CHECK-ASM-AND-OBJ: l2vertex_iall
# CHECK-ASM: encoding: [0x0b,0x00,0x60,0x01]
l2vertex_iall

# CHECK-ASM-AND-OBJ: vertex_fetch a1
# CHECK-ASM: encoding: [0x0b,0x80,0xb5,0x02]
vertex_fetch a1

# CHECK-ASM-AND-OBJ: get_buffer a1
# CHECK-ASM: encoding: [0x0b,0x80,0x35,0x02]
get_buffer a1

# CHECK-ASM-AND-OBJ: get_lod a2
# CHECK-ASM: encoding: [0x0b,0x00,0x76,0x02]
get_lod a2

# CHECK-ASM-AND-OBJ: sample_c a3
# CHECK-ASM: encoding: [0x0b,0x80,0x96,0x02]
sample_c a3

# CHECK-ASM-AND-OBJ: dvertex_cva a2
# CHECK-ASM: encoding: [0x0b,0x00,0x56,0x02]
dvertex_cva a2

# CHECK-ASM-AND-OBJ: dvertex_cval1 a2
# CHECK-ASM: encoding: [0x0b,0x00,0x46,0x02]
dvertex_cval1 a2

# CHECK-ASM-AND-OBJ: dvertex_ipa a2
# CHECK-ASM: encoding: [0x0b,0x00,0xa6,0x02]
dvertex_ipa a2

# CHECK-ASM-AND-OBJ: dvertex_isw a2
# CHECK-ASM: encoding: [0x0b,0x00,0x26,0x02]
dvertex_isw a2

# CHECK-ASM-AND-OBJ: dvertex_iva a2
# CHECK-ASM: encoding: [0x0b,0x00,0x66,0x02]
dvertex_iva a2

# CHECK-ASM-AND-OBJ: ivertex_ipa a3
# CHECK-ASM: encoding: [0x0b,0x80,0x86,0x03]
ivertex_ipa a3

# CHECK-ASM-AND-OBJ: ivertex_iva a3
# CHECK-ASM: encoding: [0x0b,0x80,0x06,0x03]
ivertex_iva a3


